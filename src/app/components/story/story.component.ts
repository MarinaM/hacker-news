import { Component, OnInit, Input } from '@angular/core';
import { StoryService } from '../../services/story/story.service';
import { Story } from '../../models/story';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.css']
})
export class StoryComponent implements OnInit {
  @Input() storyId: number;

  protected story: Story;

  protected ready: boolean = false;

  protected iconActive: boolean = false;

  protected commentsCollapsed: boolean = true;

  constructor(private storyService: StoryService) { }

  ngOnInit() {
    this.storyService.getStoryById(this.storyId).subscribe(story => {
      this.story = story;
      this.ready = true;
    });
  }

  protected toggleCommentsCollapsed() {
    this.commentsCollapsed = !this.commentsCollapsed;
  }
  protected toggleIconActive(){
    this.iconActive = !this.iconActive;
  }
}
