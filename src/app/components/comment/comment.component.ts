import { Component, OnInit, Input } from '@angular/core';
import { CommentService } from '../../services/comment/comment.service';
import { Comment } from '../../models/comment';
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input() commentId: number;

  protected ready: boolean = false;

  protected commentCollapsed: boolean = true;

  protected commentActive: boolean = false;

  protected comment: Comment;

  constructor(private commentService: CommentService) { }

  ngOnInit() {
    this.commentService.getCommentById(this.commentId).subscribe(comment => {
      this.comment = comment;
      this.ready = true;
    });
  }

  protected toggleCommentCollapsed() {
    this.commentCollapsed = !this.commentCollapsed;
  }

  protected toggleActive() {
    this.commentActive = !this.commentActive;
  }

}


