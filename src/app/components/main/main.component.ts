import { Component, OnInit } from '@angular/core';
import { StoryService } from '../../services/story/story.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  protected stories: any[] = [];

  protected p: number = 1;

  constructor(private storyService: StoryService, private spinner: NgxSpinnerService) { }

  

  ngOnInit() {
    this.storyService.getStories().subscribe(stories => {
      this.stories = stories;
    });
    this.spinner.show();
 
    setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 4000);
  }
}
