import { Component, OnInit, Input } from '@angular/core';
import { StoryService } from '../../services/story/story.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  @Input() commentIds: any[];
  @Input() storyId: number;

  constructor() { }

  ngOnInit() {

  }
}
