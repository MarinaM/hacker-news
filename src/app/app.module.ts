import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';

import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { StoryComponent } from './components/story/story.component';
import { StoryService } from './services/story/story.service';
import { CommentsComponent } from './components/comments/comments.component';
import { CommentComponent } from './components/comment/comment.component';
import { CommentService } from './services/comment/comment.service';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    StoryComponent,
    CommentsComponent,
    CommentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxPaginationModule,
    NgxSpinnerModule
  ],
  providers: [
    StoryService,
    CommentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
