export class Story {
    public by: string;
    public descendants: number;
    public id: number;
    public kids: any[];
    public score: number;
    public time: Date;
    public title: string;
    public type: string;
    public url: string;
    public commentsCount: number;

    constructor(by?: string, descendants?: number, id?: number, kids?: any[], score?: number, time?: number, title?: string, type?: string, url?: string) {
        this.by = by;
        this.descendants = descendants;
        this.id = id;
        this.kids = kids;
        this.score = score;
        this.time = new Date(time * 1000);
        this.title = title;
        this.type = type;
        this.url = url;
        this.commentsCount = !kids ? 0 : kids.length;
    }
}
