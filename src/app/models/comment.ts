export class Comment {
    public by: string;
    public id: number;
    public kids: any[];
    public parent: number;
    public text: string;
    public time: Date;
    public type: string;
    public additionalComments: number;

    constructor(by?: string, id?: number, kids?: any[], parent?: number, text?: string, time?: number, type?: string) {
        this.by = by;
        this.id = id;
        this.kids = kids;
        this.parent = parent;
        this.text = text;
        this.time = new Date(time * 1000);
        this.type = type;
        this.additionalComments = !kids ? 0 : kids.length;
    }
}

