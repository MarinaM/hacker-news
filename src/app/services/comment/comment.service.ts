import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Observer } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Comment } from '../../models/comment';
@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  public getCommentById(commentId: number) {
    return new Observable((o: Observer<any>) => {
      this.http.get(`https://hacker-news.firebaseio.com/v0/item/${commentId}.json?print=pretty`)
        .subscribe(
          (comment: any) => {
            o.next(new Comment(
              comment.by,
              comment.id,
              comment.kids,
              comment.parent,
              comment.text,
              comment.time,
              comment.type

            ));

            return o.complete();
          }, (err: HttpErrorResponse) => {
            alert(`Backend returned code ${err.status} with message: ${err.error.error}`);
          });
    });
  }
}
