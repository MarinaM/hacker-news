import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Observer } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Story } from '../../models/story';

@Injectable({
  providedIn: 'root'
})
export class StoryService {
  private stories: any[] = [];

  constructor(private http: HttpClient) { }

  public getStories() {
    return new Observable((o: Observer<any>) => {
      this.http.get('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty')
        .subscribe((stories: any[]) => {
          this.stories = stories;

          o.next(this.stories);

          return o.complete();
        }, (err: HttpErrorResponse) => {
          alert(`Backend returned code ${err.status} with message: ${err.error.error}`);
        });
    });
  }

  public getStoryById(storyId: number) {
    return new Observable((o: Observer<any>) => {
      this.http.get(`https://hacker-news.firebaseio.com/v0/item/${storyId}.json?print=pretty`)
        .subscribe(
          (story: any) => {
            o.next(new Story(
              story.by,
              story.descendants,
              story.id,
              story.kids,
              story.score,
              story.time,
              story.title,
              story.type,
              story.url

            ));

            return o.complete();
          }, (err: HttpErrorResponse) => {
            alert(`Backend returned code ${err.status} with message: ${err.error.error}`);
          });
    });
  }
}
