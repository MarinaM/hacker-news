# HackerNews

A project has the role of showing the top stories with constantly updating.
As a visitor, you can see top stories with comments on it, as well.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3. and Bootstrap 4.

## Development server

This project is based off of an Angular CLI application. Because of this, you will need to install the CLI tool (npm install -g @angular/cli) and launch this application using `ng serve`.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Project's appearance in browser

First, navigate to `http://localhost:4200/`.
    - The main view of the project:
    ![image1](/uploads/e4680f6456465b079d40522d81575313/image1.png)
    
To see a story, click on the story's title. It will be opened in a new tab:
    ![image2](/uploads/d21647919fb0fb5e70868e561f1f5866/image2.png)
    
To see a comments of the story, click on the "comment icon"
    ![image3](/uploads/04354394bf5e0b6049d0c3bab1883325/image3.png)
    
Look of the comments:
    ![image4](/uploads/d4b8af7a4520e52fc5f53a894f470eb7/image4.png)
    
The comment can has multiple comments. Look of the expanded comment:
    ![image6](/uploads/6b7d8b88fa43bfdcb67e0c1d0052384d/image6.png) 
    
    
    
    


